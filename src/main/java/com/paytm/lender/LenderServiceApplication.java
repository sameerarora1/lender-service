package com.paytm.lender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LenderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LenderServiceApplication.class, args);
    }

}
