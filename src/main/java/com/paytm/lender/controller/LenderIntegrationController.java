package com.paytm.lender.controller;

import com.paytm.lender.model.BaseVersion;
import com.paytm.lender.model.api.LenderDecisionRequest;
import com.paytm.lender.service.LenderIntegrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class LenderIntegrationController {

    @Autowired
    private LenderIntegrationService lenderIntegrationService;

    @GetMapping("/base-version")
    public BaseVersion baseVersion() {
        return new BaseVersion("1.0.0-SNAPSHOT", "Lender-service");
    }

    @RequestMapping(value = "/offers/{userId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LenderDecisionRequest> invokeLenderDecisioning(@RequestBody LenderDecisionRequest request) {
        boolean status = lenderIntegrationService.invokeLenderDecision(request);
        if (status) {
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
