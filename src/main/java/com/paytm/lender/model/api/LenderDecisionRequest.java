package com.paytm.lender.model.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paytm.lender.model.kyc.KYCInformation;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LenderDecisionRequest {

    @JsonProperty("source")
    private String source;
    @JsonProperty("product_id")
    private Integer productId;
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("lead_id")
    private String leadId;
    @JsonProperty("loan_application_id")
    private String loanApplicationId;
    @JsonProperty("kyc_info")
    private KYCInformation kycInformation;
    @JsonProperty("feature_variables")
    private Map<String, String> featureVariables;
    @JsonProperty("offer_details")
    private Map<String, String> offerDetails;
    @JsonProperty("callback_url")
    private String callBackUrl;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getLoanApplicationId() {
        return loanApplicationId;
    }

    public void setLoanApplicationId(String loanApplicationId) {
        this.loanApplicationId = loanApplicationId;
    }

    public KYCInformation getKycInformation() {
        return kycInformation;
    }

    public void setKycInformation(KYCInformation kycInformation) {
        this.kycInformation = kycInformation;
    }

    public Map<String, String> getFeatureVariables() {
        return featureVariables;
    }

    public void setFeatureVariables(Map<String, String> featureVariables) {
        this.featureVariables = featureVariables;
    }

    public Map<String, String> getOfferDetails() {
        return offerDetails;
    }

    public void setOfferDetails(Map<String, String> offerDetails) {
        this.offerDetails = offerDetails;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }
}
