package com.paytm.lender.model.mapper;


import com.paytm.lender.model.bo.LenderConfigBO;
import com.paytm.lender.model.config.LenderConfigEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LenderConfigMapper {

    LenderConfigMapper INSTANCE = Mappers.getMapper(LenderConfigMapper.class);

    LenderConfigBO entityToBo(LenderConfigEntity entity);

    LenderConfigEntity boToEntity(LenderConfigBO bo);
}