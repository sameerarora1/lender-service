package com.paytm.lender.model.config;


import com.paytm.lender.model.enums.LenderEnum;
import com.paytm.lender.model.enums.ProductTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@ToString
public class LenderConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "product_type")
    private ProductTypeEnum productType;

    @Enumerated(EnumType.STRING)
    @Column(name = "lender")
    private LenderEnum lender;

    @Column(name = "percentage")
    private Integer percentage;

    @Column(name = "lender_id")
    private Integer lenderId;
}
