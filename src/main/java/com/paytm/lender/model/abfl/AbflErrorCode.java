package com.paytm.lender.model.abfl;

public enum AbflErrorCode {
    INVALID_CREDENTIALS, INTERNAL_SERVER_ERROR
}