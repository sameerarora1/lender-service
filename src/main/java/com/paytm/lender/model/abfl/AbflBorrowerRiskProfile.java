package com.paytm.lender.model.abfl;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class AbflBorrowerRiskProfile {
    private Integer highRisk;
}
