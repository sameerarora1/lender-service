package com.paytm.lender.model.abfl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class AbflError {
private AbflErrorCode errorCode;
private String description;
}
