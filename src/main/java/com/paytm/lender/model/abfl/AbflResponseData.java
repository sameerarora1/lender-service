package com.paytm.lender.model.abfl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class AbflResponseData {
    private String token;
}
