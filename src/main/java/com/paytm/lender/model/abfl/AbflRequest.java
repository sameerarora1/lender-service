package com.paytm.lender.model.abfl;

import com.google.gson.Gson;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AbflRequest {
    private String source;
    private String accountId;
    private String productCode;
    private AbflCustomerReport customerReport;
    private boolean delinkExisting;

    @Override
    public String toString(){
        return new Gson().toJson(this);
    }

}
