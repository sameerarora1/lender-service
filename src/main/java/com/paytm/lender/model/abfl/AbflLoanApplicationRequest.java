package com.paytm.lender.model.abfl;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class AbflLoanApplicationRequest {

private Double requestedLoanAmount;
private String productVariant;
private String spendsCategory;
private String convenienceFeeCategory;
private String spendCategoryMaxLimit;
}
