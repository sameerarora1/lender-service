package com.paytm.lender.model.abfl;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AbflCustomerReport {
    private AbflBorrowerRiskProfile borrowerRiskProfile;
    private AbflKycInfo kycInfo;
    private AbflLoanApplicationRequest loanApplicationRequest;

}
