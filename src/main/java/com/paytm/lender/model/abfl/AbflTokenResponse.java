package com.paytm.lender.model.abfl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

public class AbflTokenResponse {
private String responseStatus;
private AbflResponseData data;
private AbflError error;
}
