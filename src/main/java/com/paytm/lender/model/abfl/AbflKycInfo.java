package com.paytm.lender.model.abfl;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class AbflKycInfo {
   private String entityType;
   private String firstName;
    private String lastName;
    private String mobile;
    private String email;
    private String panNumber;
    private String gender;
    private String dob;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String city;
    private String state;
    private Integer pincode;
}
