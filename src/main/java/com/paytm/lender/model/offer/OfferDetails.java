package com.paytm.lender.model.offer;

public class OfferDetails {
    private String leadId;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }
}
