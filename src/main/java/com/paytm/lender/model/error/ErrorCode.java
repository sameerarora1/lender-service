package com.paytm.lender.model.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class ErrorCode {
    private ErrorCodeEnum errorCodeEnum;
    private String message;

    public ErrorCode(final ErrorCodeEnum errorCodeEnum) {
        this.errorCodeEnum = errorCodeEnum;
        this.message = errorCodeEnum.getMessage();
    }

    public void setMessage(String message) {
        this.message = message;
    }
}