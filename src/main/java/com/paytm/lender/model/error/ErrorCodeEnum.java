package com.paytm.lender.model.error;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public enum ErrorCodeEnum {

  NOT_FOUND(
      "NOT_FOUND",
      4001,
      "Resource not found.",
      HttpStatus.NOT_FOUND
  ),

  INVALID_REQUEST(
      "INVALID_REQUEST",
      5001,
      "Invalid request.",
      HttpStatus.BAD_REQUEST
  ),
  BAD_REQUEST(
      "BAD_REQUEST",
      5002,
      "Invalid data sent.",
      HttpStatus.BAD_REQUEST
  ),
  BAD_REQUEST_SSO_TOKEN_MISSING(
      "BAD_REQUEST_SSO_TOKEN_MISSING",
      5004,
      "Invalid data sent, sso token missing",
      HttpStatus.BAD_REQUEST
  ),

  UNABLE_TO_PROCESS(
      "UNABLE_TO_PROCESS",
      6001,
      "Unable to process current request.",
      HttpStatus.UNPROCESSABLE_ENTITY
  ),
  BUREAU_REPORT_MISSING(
      "UNABLE_TO_PROCESS",
      6002,
      "Unable to find bureau report",
      HttpStatus.UNPROCESSABLE_ENTITY
  ),
  BUREAU_XML_MISSING(
          "UNABLE_TO_PROCESS",
          6002,
          "Unable to find bureau xml",
          HttpStatus.FAILED_DEPENDENCY
  ),
  UNABLE_TO_ADD_PARAMS(
      "UNABLE_TO_ADD_PARAMS",
      6007,
      "Unable to add params.",
      HttpStatus.UNPROCESSABLE_ENTITY
  ),
  UNABLE_TO_CALL_HTTP(
      "UNABLE_TO_CALL_HTTP",
      6008,
      "Not able to call http.",
      HttpStatus.UNPROCESSABLE_ENTITY
  ),
  UNABLE_TO_JSONIZE(
      "UNABLE_TO_JSONIZE",
      6009,
      "Not able to jsonize data.",
      HttpStatus.UNPROCESSABLE_ENTITY
  ),
  UNABLE_TO_PROCESS_RESPONSE_FROM_LENDER(
          "UNABLE_TO_PROCESS_RESPONSE_FROM_LENDER",
          6010,
          "lender response is not valid",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  UNABLE_TO_PROCESS_LENDER_PF_MISMATCH(
          "UNABLE_TO_PROCESS_LENDER_PF_MISMATCH",
          6050,
          "Pf provided by lender doesn't match Paytm offer slab",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),

  UNABLE_TO_PROCESS_LENDER_ROI_MISMATCH(
          "UNABLE_TO_PROCESS_LENDER_ROI_MISMATCH",
          6051,
          " ROI provided by lender doesn't match Paytm offer slab",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),

  UNABLE_TO_PROCESS_LENDER_TENURE_MISMATCH(
          "UNABLE_TO_PROCESS_LENDER_TENURE_MISMATCH",
          6010,
          "Tenure provided by lender doesn't match Paytm offer slab",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  UNABLE_TO_GET_DISTANCE(
      "UNABLE_TO_GET_DISTANCE",
      6010,
      "Not able to get distance.",
      HttpStatus.UNPROCESSABLE_ENTITY
  ),
  NON_SUCCESS_REQUEST(
          "UNABLE_TO_PROCESS",
          6011,
          "Got non success input",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  UNAUTHORIZED_ACCESS(
      "UNAUTHORIZED_ACCESS",
      70001,
      "Not authorized to perform this action.",
      HttpStatus.UNAUTHORIZED
  ),
  NON_EXISTING_USER(
      "NON_EXISTING_USER",
      70002,
      "User does not exist to perform this action.",
      HttpStatus.UNAUTHORIZED
  ),

  LEAD_NOT_PRESENT(
      "LEAD_NOT_PRESENT",
      70002,
      "Lead not exist",
      HttpStatus.NO_CONTENT
  ),

  INTERNAL_SERVER_ERROR(
      "INTERNAL_SERVER_ERROR",
      80001,
      "Unknown error happened.",
      HttpStatus.INTERNAL_SERVER_ERROR
  ),

  SYSTEM_IN_MAINTENANCE_MODE(
      "SYSTEM_IN_MAINTENANCE_MODE",
      90001,
      "System is under maintenance mode.",
      HttpStatus.SERVICE_UNAVAILABLE
  ),

  BRE_CHECK_FAIL(
      "BRE CHECK FAIL",
      90002,
      "Bre check fail",
      HttpStatus.OK
  ),

  PDC_CHECK_FAIL(
      "PDC CHECK FAIL",
      90002,
      "pdc check fail",
      HttpStatus.OK
  ),

  EQC_CHECK_FAIL(
      "EQC CHECK FAIL",
      90002,
      "EQC check fail",
      HttpStatus.OK
  ),

  APPLICATION_REJECTED(
      "Application rejected",
      90002,
      "Application rejected",
      HttpStatus.OK
  ),

  PROCESSING(
      "Application is in progress",
          90006,
          "Application is in progress",
      HttpStatus.UNPROCESSABLE_ENTITY
      ),
  PROCESSING_OK(
          "Application is in progress",
          90006,
          "Application is in progress",
          HttpStatus.OK
  ),

  WHITELIST_CHECK_FAIL(
          "WHITELIST CHECK FAIL",
          90002,
          "whitelist check fail",
          HttpStatus.OK
  ),
  APPLICATION_WAITLIST(
		  "Application in waitlist",
		  90006,
          "Application in waitlist",
          HttpStatus.OK
		  ),
  NO_CONTENT(
		  "No Content",
          20004,
          "No Content",
          HttpStatus.NO_CONTENT
		  ),

  MISSING_CUSTOMER_DATA(
          "UNABLE_TO_PROCESS",
          20006,
          "Customer Data Missing in One or More Places",
          HttpStatus.NOT_FOUND
  ),
  MISSING_ONBOARDING_DATE(
          "UNABLE_TO_PROCESS",
          20007,
          "onboarding date Missing in merchant details",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  MISSING_MERCHANT_TYPE(
          "UNABLE_TO_PROCESS",
          20008,
          "Merchant Type missing in merchant details",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  MISSING_SUB_INDUSTRY_CATEGORY(
          "UNABLE_TO_PROCESS",
          20009,
          "Sub Category is not available",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  MISSING_PAYTM_SCORE(
          "UNABLE_TO_PROCESS",
          20010,
          "Paytm Score is not available",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  MISSING_MERCHANT_TXN_DATA(
          "UNABLE_TO_PROCESS",
          20010,
          "Merchant Txn Data is not available",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  MISSING_MERCHANT_DETAILS(
          "UNABLE_TO_PROCESS",
          20011,
          "merchant details not available",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  LENDER_DECISION_CALL_FAILED(
          "UNABLE_TO_PROCESS",
          20011,
          "Lender Decision Call Failed.",
          HttpStatus.UNPROCESSABLE_ENTITY
  ),
  UNKNOWN_SERVICE_TYPE(
          "unknown service type",
                  20005,
                  "unknown service type",
          HttpStatus.NOT_FOUND
          ),
  LIMIT_ASSIGNMENT_FAILED(
          "Limit Assignment failued",
                  20006,
                  "Limit Assignment failued",
          HttpStatus.OK
          ),
  CONNECTION_TIMEDOUT(
          "Connection Timed Out",
          6001,
          "Connection Timed Out",
          HttpStatus.REQUEST_TIMEOUT
          ),
  CALLBACK_FAILED(
          "Callback Failed",
          20006,
          "Callback Failed",
          HttpStatus.INTERNAL_SERVER_ERROR
  );

  private Integer code;
  private String status;
  private String message;
  private HttpStatus httpStatus;

  ErrorCodeEnum(String message) {
    this.message = message;
  }

  ErrorCodeEnum(final String status, final Integer code, final String message,
      final HttpStatus httpStatus) {
    this.status = status;
    this.code = code;
    this.message = message;
    this.httpStatus = httpStatus;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
