package com.paytm.lender.model.enums;

import com.paytm.lender.model.error.ErrorCode;
import com.paytm.lender.model.error.ErrorCodeEnum;
import com.paytm.lender.model.exception.BaseException;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


@Getter
@ToString
public enum LenderEnum {
    CLIX("CLIX", "Clix Capital", 1L),
    HAPPY("HAPPY_LOANS","Happy Loans",2L),
    SSFB("SURYODAY_BANK", "Suryoday Bank", 3L),
    MSL("MY_SHUBH_LIFE","My Shubh Life",4L),
    ABFL("ADITYA_BIRLA","Aditya Birla Finance Limited", 5L),
    SBI("SBI","State Bank Of India", 6L),
    CITI("CITI","Citi Bank", 7L),
    HERO("HERO","Hero",8L);

    private static final Map<String, LenderEnum> REVERSE_MAP_BY_CODE;

    static {
        REVERSE_MAP_BY_CODE = Arrays.stream(LenderEnum.values())
                .collect(Collectors.toMap(LenderEnum::getCode, Function.identity()));
    }

    private String code;
    private String name;
    private Long id;

    LenderEnum(final String code, final String name, final Long id) {
        this.code = code;
        this.name = name;
        this.id = id;
    }


    public static LenderEnum getLenderByCode(final String code) {
        if (StringUtils.isEmpty(code)) {
            throw new BaseException(new ErrorCode(ErrorCodeEnum.INTERNAL_SERVER_ERROR, "Lender code not found"));
        }

        return REVERSE_MAP_BY_CODE.get(code);
    }
}
