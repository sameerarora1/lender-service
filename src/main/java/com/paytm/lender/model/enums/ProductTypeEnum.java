package com.paytm.lender.model.enums;

import com.paytm.lender.model.error.ErrorCode;
import com.paytm.lender.model.error.ErrorCodeEnum;
import com.paytm.lender.model.exception.BaseException;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


@Getter
@ToString
public enum ProductTypeEnum {
    PL("PL", "Personal Loan", 1L),
    POSTPAID("POSTPAID","Postpaid Loan",2L),
    MCA("MCA","MCA",3L),
    BBL("BBL","BBL",4L),
    PP_EMI("PP_EMI","PP_EMI",5L),
    ARIL("ARIL","ARIL",6L),
    ARIL_EDC("ARIL_EDC","ARIL_EDC",7L),
	CREDIT_CARD("CREDIT_CARD","CREDIT_CARD",8L);

    private static final Map<String, ProductTypeEnum> REVERSE_MAP_BY_CODE;

    private static final Map<String, ProductTypeEnum> REVERSE_MAP_BY_NAME;

    static {
        REVERSE_MAP_BY_CODE = Arrays.stream(ProductTypeEnum.values())
                .collect(Collectors.toMap(ProductTypeEnum::getCode, Function.identity()));

        REVERSE_MAP_BY_NAME = Arrays.stream(ProductTypeEnum.values())
                .collect(Collectors.toMap(ProductTypeEnum::name, Function.identity()));
    }

    private String code;
    private String label;
    private Long id;

    ProductTypeEnum(final String code, final String label, final Long id) {
        this.id = id;
        this.code = code;
        this.label = label;
    }

    public static ProductTypeEnum getProductTypeByCode(final String code) {
        if (StringUtils.isEmpty(code)) {
            throw new BaseException(new ErrorCode(ErrorCodeEnum.UNABLE_TO_PROCESS));
        }
        return REVERSE_MAP_BY_CODE.get(code);
    }

    public static ProductTypeEnum getProductTypeByName(final String productType) {
        if (productType == null) {
            throw new BaseException();
        }

        return REVERSE_MAP_BY_NAME.get(productType);
    }
}
