package com.paytm.lender.model.exception;

import com.paytm.lender.model.error.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BaseException extends RuntimeException {
  private ErrorCode errorCode;
}
