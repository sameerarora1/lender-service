package com.paytm.lender.model.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KYCInformation {

    @JsonProperty("user")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
