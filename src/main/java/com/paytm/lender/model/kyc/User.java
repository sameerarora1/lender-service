package com.paytm.lender.model.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class User {

    private List<Address> addresses;

    private String dob;

    private String occupation;
    private String monthlyIncome;
    private Double annualSalary;
    private String employerName;
    private String companyName;
    private String middleName;
    private String id;
    private String gender;
    private String pan;
    private String mobile;
    private String email;
    private String firstName;
    private String lastName;
    @JsonProperty("lender_cust_id")
    private String lenderCustomerId;
    private String fatherFName;
    private String fatherLName;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(String monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public Double getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(Double annualSalary) {
        this.annualSalary = annualSalary;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLenderCustomerId() {
        return lenderCustomerId;
    }

    public void setLenderCustomerId(String lenderCustomerId) {
        this.lenderCustomerId = lenderCustomerId;
    }

    public String getFatherFName() {
        return fatherFName;
    }

    public void setFatherFName(String fatherFName) {
        this.fatherFName = fatherFName;
    }

    public String getFatherLName() {
        return fatherLName;
    }

    public void setFatherLName(String fatherLName) {
        this.fatherLName = fatherLName;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
