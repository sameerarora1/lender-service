package com.paytm.lender.repository;

import com.paytm.lender.model.config.RiskConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RiskConfigRepository extends JpaRepository<RiskConfigEntity, Long> {

	RiskConfigEntity findByConfigKey(String configKey);

}
