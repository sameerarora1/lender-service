package com.paytm.lender.repository;

import com.paytm.lender.model.config.LenderConfigEntity;
import com.paytm.lender.model.enums.ProductTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LenderConfigRepository extends JpaRepository<LenderConfigEntity, Long> {
    List<LenderConfigEntity> findByProductType(ProductTypeEnum productType);

    List<LenderConfigEntity> findAll();
}
