package com.paytm.lender.service;

import com.paytm.lender.model.api.LenderDecisionRequest;

public interface LenderIntegrationService {
    boolean invokeLenderDecision(LenderDecisionRequest request);
}
