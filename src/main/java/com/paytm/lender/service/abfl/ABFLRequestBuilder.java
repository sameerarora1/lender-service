package com.paytm.lender.service.abfl;

import com.paytm.lender.model.abfl.*;
import com.paytm.lender.model.api.LenderDecisionRequest;
import com.paytm.lender.model.error.ErrorCode;
import com.paytm.lender.model.error.ErrorCodeEnum;
import com.paytm.lender.model.exception.BaseException;
import com.paytm.lender.model.kyc.KYCInformation;
import com.paytm.lender.model.kyc.User;
import com.paytm.lender.repository.RiskConfigRepository;
import com.paytm.lender.service.utils.PostpaidLoanDetailsCalculatorUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import static com.paytm.lender.service.abfl.FeatureConstants.*;
import static com.paytm.lender.service.utils.Constants.MSL_PP;
import static com.paytm.lender.service.utils.Constants.PAYTM;
import static com.paytm.lender.service.utils.PostpaidLoanDetailsCalculatorUtil.calculateConvenienceFeeCategory;
import static com.paytm.lender.service.utils.PostpaidLoanDetailsCalculatorUtil.calculateSpendCategoryMaxLimit;
import static java.lang.Double.parseDouble;

@Component
@Slf4j
public class ABFLRequestBuilder {

    @Autowired
    RiskConfigRepository riskConfigRepo;
    private static final String PP_CLIX_SPEND_CATEGORY_CODE = "PP_CLIX_SPEND_CATEGORY_CODE";

    public AbflRequest build(LenderDecisionRequest lenderDecisionRequest) {
        try {
            return createRequestDto(lenderDecisionRequest);
        } catch (Exception e) {
            throw new BaseException(new ErrorCode(ErrorCodeEnum.BAD_REQUEST, "ABFL Request could not be formed due to malformed input"));
        }
    }

    private AbflRequest createRequestDto(LenderDecisionRequest lenderDecisionRequest) throws Exception {
        AbflCustomerReport customerReport = getAbflCustomerReport(lenderDecisionRequest);
        return AbflRequest.builder()
                .source(PAYTM)
                .productCode(MSL_PP)
                .accountId(lenderDecisionRequest.getLeadId())
                .customerReport(customerReport)
                .delinkExisting(true)
                .build();
    }

    private AbflCustomerReport getAbflCustomerReport(LenderDecisionRequest lenderDecisionRequest) throws Exception {
        AbflKycInfo kycInfo = buildKycInfo(lenderDecisionRequest);
        AbflLoanApplicationRequest loanApplicationRequest = buildLoanApplicationRequest(lenderDecisionRequest);
        AbflBorrowerRiskProfile abflBorrowerRiskProfile = getAbflBorrowerRiskProfile(lenderDecisionRequest);

        AbflCustomerReport customerReport = buildCustomerReport(abflBorrowerRiskProfile, kycInfo, loanApplicationRequest);
        return customerReport;
    }

    private AbflBorrowerRiskProfile getAbflBorrowerRiskProfile(LenderDecisionRequest lenderDecisionRequest) {
        Map<String, String> featureVariables = lenderDecisionRequest.getFeatureVariables();
        Integer riskProfile = Integer.parseInt(featureVariables.get("risk_profile"));
        AbflBorrowerRiskProfile borrowerRiskProfile = AbflBorrowerRiskProfile.builder().highRisk(riskProfile).build();
        return borrowerRiskProfile;
    }

    private AbflCustomerReport buildCustomerReport(AbflBorrowerRiskProfile riskProfile, AbflKycInfo kycInfo, AbflLoanApplicationRequest loanApplicationRequest) {
        AbflCustomerReport report = AbflCustomerReport.builder()
                .borrowerRiskProfile(riskProfile)
                .kycInfo(kycInfo)
                .loanApplicationRequest(loanApplicationRequest)
                .build();
        return report;
    }

    private AbflLoanApplicationRequest buildLoanApplicationRequest(LenderDecisionRequest lenderDecisionRequest) throws Exception {
        Map<String, String> featureVariables = lenderDecisionRequest.getFeatureVariables();
        double calculatedLoanAmount = parseDouble(featureVariables.get(CALCULATED_LOAN_AMOUNT));
        AbflLoanApplicationRequest loanApplicationRequest = AbflLoanApplicationRequest.builder()
                .requestedLoanAmount(calculatedLoanAmount)
                .convenienceFeeCategory(calculateConvenienceFeeCategory(parseDouble(featureVariables.get(CONVENIENCE_FEE))))
                .productVariant(featureVariables.get(PRODUCT_VARIANT_ID))
                .spendCategoryMaxLimit(calculateSpendCategoryMaxLimit(calculatedLoanAmount, riskConfigRepo
                        .findByConfigKey(PP_CLIX_SPEND_CATEGORY_CODE)))
                .spendsCategory(PostpaidLoanDetailsCalculatorUtil.calculateSpendCategory(calculatedLoanAmount))
                .build();
        return loanApplicationRequest;
    }

    private AbflKycInfo buildKycInfo(LenderDecisionRequest lenderDecisionRequest) throws Exception {
        KYCInformation kycInformation = lenderDecisionRequest.getKycInformation();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dobFormat = new SimpleDateFormat("dd-MM-yyyy");
        User user = kycInformation.getUser();
        AbflKycInfo kycInfo = AbflKycInfo.builder()
                .state(user.getAddresses().get(0).getState())
                .city(user.getAddresses().get(0).getCity())
                .addressLine1(user.getAddresses().get(0).getLine1() == null ? "" : user.getAddresses().get(0).getLine1())
                .addressLine2(user.getAddresses().get(0).getLine2() == null ? "" : user.getAddresses().get(0).getLine2())
                .addressLine3("")
                .email(user.getEmail())
                .entityType("I")
                .firstName(user.getFirstName())
                .gender("MALE".equalsIgnoreCase(user.getGender().trim()) ||
                        "M".equalsIgnoreCase(user.getGender().trim()) ? "M" : "F")
                .lastName(user.getLastName())
                .mobile(user.getMobile())
                .panNumber(user.getPan())
                .pincode(Integer.parseInt(user.getAddresses().get(0).getPincode()))
                .build();

        try {
            kycInfo.setDob(sdf.format(dobFormat.parse(user.getDob())));
        } catch (ParseException e) {
            log.error("invalid date format bad request");
            log.error("dob is in invalid format: {}", user.getDob(), e);
            throw new Exception(e.getMessage());
        }
        return kycInfo;
    }

}
