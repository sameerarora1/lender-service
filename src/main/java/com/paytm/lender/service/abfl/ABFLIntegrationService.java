package com.paytm.lender.service.abfl;

import com.paytm.lender.model.abfl.AbflRequest;
import com.paytm.lender.model.abfl.AbflTokenResponse;
import com.paytm.lender.model.api.LenderDecisionRequest;
import com.paytm.lender.model.error.ErrorCode;
import com.paytm.lender.model.error.ErrorCodeEnum;
import com.paytm.lender.model.exception.BaseException;
import com.paytm.lender.service.LenderIntegrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ABFLIntegrationService implements LenderIntegrationService {

    @Autowired
    private ABFLRequestBuilder requestBuilder;

    @Autowired
    private ABFLClient abflClient;

    @Override
    public boolean invokeLenderDecision(LenderDecisionRequest request) {
        AbflRequest abflRequest = requestBuilder.build(request);
        AbflTokenResponse abflClientToken = abflClient.createToken();
        ResponseEntity<String> abflDecisionResponse = abflClient
                .abflDecisionCheck(abflRequest,
                        abflClientToken.getData().getToken());
        if (!abflDecisionResponse.getStatusCode().equals(HttpStatus.OK)) {
            log.error("ABFL lender call fail");
            throw new BaseException(new ErrorCode(ErrorCodeEnum.INTERNAL_SERVER_ERROR, "Lender Call Failed"));
        }
        return true;
    }

}
