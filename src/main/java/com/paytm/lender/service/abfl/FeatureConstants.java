package com.paytm.lender.service.abfl;

public class FeatureConstants {
    public static final String CALCULATED_LOAN_AMOUNT = "calculated_loan_amount";
    public static final String CONVENIENCE_FEE = "convenience_fee";
    public static final String PRODUCT_VARIANT_ID = "product_variant_id";
}
