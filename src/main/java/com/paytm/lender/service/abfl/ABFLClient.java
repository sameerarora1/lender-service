package com.paytm.lender.service.abfl;

import com.paytm.lender.model.abfl.AbflRequest;
import com.paytm.lender.model.abfl.AbflTokenResponse;
import com.paytm.lender.model.error.ErrorCode;
import com.paytm.lender.model.error.ErrorCodeEnum;
import com.paytm.lender.model.exception.BaseException;
import com.paytm.lender.service.utils.HttpClient;
import com.paytm.lender.service.utils.ResponseMapper;
import com.paytm.lender.service.utils.RestUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class ABFLClient {

    public boolean invokeLender(AbflRequest abflRequest) {
        return false;
    }

    @Autowired
    private RestUtils restUtils;

    @Autowired
    private ResponseMapper mapper;

    @Value("${abfl.base.url}")
    private String abflBaseUrl;

    @Value("${abfl.createToken.url}")
    private String createTokenUrl;

    @Value("${abfl.createToken.username}")
    private String userName;

    @Value("${abfl.createToken.password}")
    private String pass;

    @Value("${abfl.decisionEngine.url}")
    private String decisionUrl;

    @Autowired
    private HttpClient httpClient;

    private static HttpComponentsClientHttpRequestFactory clientHttpRequestFactory;

    @PostConstruct
    private void loadHttpClientFactory(){
        clientHttpRequestFactory=httpClient.getHttpClientFactory();
    }

    public AbflTokenResponse createToken() {

        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        String url = new StringBuilder().append(abflBaseUrl).append(createTokenUrl).toString();

        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("username",userName);
        queryParam.put("password",pass);


        log.info("creating new Auth token for ABFL with url: {}", url);

        try {
            ResponseEntity<String> data = restUtils
                    .restPostCall(clientHttpRequestFactory,url, headers, null, mapper.convertObjectIntoString(queryParam));

            log.info("Abfl auth token received for PL response body {}", data.getBody());
            AbflTokenResponse response=mapper.parseResponse(data.getBody(),AbflTokenResponse.class);
            if(response.getError()!=null){
                log.error("error encountered in creating ABFL token: {}",response.getError());
                throw new BaseException(new ErrorCode(ErrorCodeEnum.UNABLE_TO_PROCESS));
            }
            return response;
        } catch (Exception ex) {
            log.error("Error while calling Delphi for PP : ", ex);
            throw new BaseException(new ErrorCode(ErrorCodeEnum.UNABLE_TO_PROCESS));
        }

    }
    public ResponseEntity<String> abflDecisionCheck(AbflRequest request, String token) {

        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.set("X-AUTH-TOKEN",token);
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        String url = new StringBuilder().append(abflBaseUrl).append(decisionUrl).toString();

        Map<String, String> queryParam = new HashMap<>();


        String body = mapper.convertObjectIntoString(request);

        log.info("ABFL Decision check for PP request url {} params {} body {}", url, queryParam, body);

        try {
            ResponseEntity<String> data = restUtils
                    .restPostCall(clientHttpRequestFactory,url, headers, queryParam, body);

            log.info("ABFL Decision check for PP response body {}", data.getBody());

            return data;
        } catch (Exception ex) {
            log.error("Error while calling ABFL Client for Postpaid : ", ex);
            throw ex;
        }

    }
}
