package com.paytm.lender.service;


import com.paytm.lender.model.bo.LenderConfigBO;
import com.paytm.lender.model.config.LenderConfigEntity;
import com.paytm.lender.model.enums.ProductTypeEnum;
import com.paytm.lender.repository.LenderConfigRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.paytm.lender.model.mapper.LenderConfigMapper.INSTANCE;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class LenderConfigServiceImpl {

    @Autowired
    private LenderConfigRepository lenderConfigRepo;

    public List<LenderConfigBO> getLenderConfigs() {
        return mapToLenderConfigBO(lenderConfigRepo.findAll());
    }


    public List<LenderConfigBO> getLenderConfigByProductType(ProductTypeEnum productType) {
        return mapToLenderConfigBO(lenderConfigRepo.findByProductType(productType));
    }

    private List<LenderConfigBO> mapToLenderConfigBO(List<LenderConfigEntity> entities) {
        return entities != null ? entities.stream().map(INSTANCE::entityToBo).collect(toList()) : new ArrayList<>();
    }
}
