package com.paytm.lender.service.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class RestUtils {

  /**
   * generic GET Rest API Call.
   *
   * @param url .
   * @param headers .
   * @param queryParamMap .
   * @return response .
   */
  public ResponseEntity<String> restGetCall(final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory,
                                            final String url, final HttpHeaders headers,
                                            final Map<String, String> queryParamMap) {

    HttpEntity<?> entity = new HttpEntity<>(headers);
    ResponseEntity<String> response;
    String uri = url;
    if (null != queryParamMap && !queryParamMap.isEmpty()) {
      try {
        uri = addParameters(queryParamMap, uri);
      } catch (IOException ex) {
        log.error("Exception: ",ex);
      }
    }
    final RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);

    return restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
  }

  /**
   * generic PUT Rest API Call.
   *
   * @param url .
   * @param headers .
   * @param queryParamMap .
   * @return response .
   */
  public ResponseEntity<String> restPutCall(final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory,
                                            final String url, final HttpHeaders headers,
      final Map<String, String> queryParamMap, final String requestBody) {

    HttpEntity<?> entity = new HttpEntity<Object>(requestBody, headers);
    ResponseEntity<String> response;
    String uri = url;
    if (null != queryParamMap && !queryParamMap.isEmpty()) {
      try {
        uri = addParameters(queryParamMap, uri);
      } catch (IOException ex) {
        log.error("Exception: ",ex);
      }
    }
    final RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
    return restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
  }

  /**
   * generic POST Rest API Call.
   *
   * @param url .
   * @param headers .
   * @param queryParamMap .
   * @return response .
   */
  public ResponseEntity<String> restPostCall(final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory,final String url, final HttpHeaders headers,
      final Map<String, String> queryParamMap, final String requestBody) {

    final HttpEntity<?> entity = new HttpEntity<Object>(requestBody, headers);
    // HttpEntity<String> response;

    String uri = url;
    if (null != queryParamMap && !queryParamMap.isEmpty()) {
      try {
        uri = addParameters(queryParamMap, uri);
      } catch (IOException ex) {
        log.error("Exception: ",ex);
      }
    }
    final RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
    final ResponseEntity<String> response =
        restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    log.info("Response received is : {}", response);
    return response;
  }

  /**
   * generic DELETE Rest API Call.
   *
   * @param url .
   * @param headers .
   * @param queryParamMap .
   * @return response .
   */
  public ResponseEntity<String> restDeleteCall(final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory,final String url, final HttpHeaders headers,
      final Map<String, String> queryParamMap, final String requestBody) {

    HttpEntity<?> entity = new HttpEntity<Object>(requestBody, headers);
    ResponseEntity<String> response;
    String uri = url;
    if (null != queryParamMap && !queryParamMap.isEmpty()) {
      try {
        uri = addParameters(queryParamMap, uri);
      } catch (IOException ex) {
        log.error("Exception: ",ex);
      }
    }
    final RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
    return restTemplate.exchange(uri, HttpMethod.DELETE, entity, String.class);
  }

  /**
   * generic PATCH Rest API Call.
   *
   * @param url .
   * @param headers .
   * @param queryParamMap .
   * @return response .
   */
  public ResponseEntity<String> restPatchCall(final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory,final String url, final HttpHeaders headers,
      final Map<String, String> queryParamMap, final String requestBody) {

    HttpEntity<?> entity = new HttpEntity<Object>(requestBody, headers);
    ResponseEntity<String> response;
    String uri = url;
    if (null != queryParamMap && !queryParamMap.isEmpty()) {
      try {
        uri = addParameters(queryParamMap, uri);
      } catch (IOException ex) {
        log.error("Exception: ",ex);
      }
    }
    final RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
    return restTemplate.exchange(uri, HttpMethod.PATCH, entity, String.class);
  }

  /**
   * generic HEAD Rest API Call.
   *
   * @param url .
   * @param headers .
   * @param queryParamMap .
   * @return response .
   */
  public ResponseEntity<String> restHeadCall(final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory,final String url, final HttpHeaders headers,
      final Map<String, String> queryParamMap) {

    HttpEntity<?> entity = new HttpEntity<>(headers);
    ResponseEntity<String> response;
    String uri = url;
    if (null != queryParamMap && !queryParamMap.isEmpty()) {
      try {
        uri = addParameters(queryParamMap, uri);
      } catch (IOException ex) {
        log.error("Exception: ",ex);
      }
    }
    final RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
    return restTemplate.exchange(uri, HttpMethod.HEAD, entity, String.class);
  }


  private String addParameters(final Map<String, String> map, final String url)
      throws IOException {
    try {
      List<NameValuePair> queryParams = new ArrayList<>();
      for (Map.Entry<String, String> entry : map.entrySet()) {
        queryParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
      }
      return new URIBuilder(url).addParameters(queryParams).build().toString();
    } catch (URISyntaxException ex) {
      // log.error("malformed URL {}", url, ex);
      throw new IOException(ex);
    }
  }

}
