package com.paytm.lender.service.utils;

import com.paytm.lender.model.error.ErrorCode;
import com.paytm.lender.model.error.ErrorCodeEnum;
import com.paytm.lender.model.exception.BaseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class ResponseMapper {

  private ObjectMapper objectMapper =new ObjectMapper();

  public <T> T validateAndParseResponse(ResponseEntity<String> responseEntity,
      final Class<T> responseClass) throws BaseException {

    if (HttpStatus.OK.value() != responseEntity.getStatusCode().value()) {
      log.info("Non 200 response received .. exiting process and returning response");
      throw new BaseException(new ErrorCode(ErrorCodeEnum.INTERNAL_SERVER_ERROR));
    }

    return parseResponse(responseEntity.getBody(), responseClass);
  }

  public <T> T validateCreatedAndParseResponse(ResponseEntity<String> responseEntity,
      final Class<T> responseClass) throws BaseException {

    if (HttpStatus.CREATED.value() != responseEntity.getStatusCode().value()) {
      log.info("Non 201 response received .. exiting process and returning response");
      throw new BaseException(new ErrorCode(ErrorCodeEnum.INTERNAL_SERVER_ERROR));
    }

    return parseResponse(responseEntity.getBody(), responseClass);
  }
  
  public <T> T parseResponse(String responseString, final Class<T> responseClass)
      throws BaseException {

    try {
      return objectMapper.readValue(responseString, responseClass);
    } catch (JsonProcessingException e) {
      log.error("could not parse to class : [{}], from string : [{}]", responseClass,
          responseString);
      log.error("Exception occurred while parsing response : ", e);
      throw new BaseException(new ErrorCode(ErrorCodeEnum.INTERNAL_SERVER_ERROR));
    }
  }

  public <T> String convertObjectIntoString(Object requestObject) throws BaseException {

    try {
      return objectMapper.writeValueAsString(requestObject);
    } catch (JsonProcessingException e) {
      log.error("Exception: ",e);
      log.error("could not convert object to string, from string : [{}]", requestObject);
      log.error("Exception occurred while parsing response : ", e);
      throw new BaseException(new ErrorCode(ErrorCodeEnum.INTERNAL_SERVER_ERROR));
    }

  }
}
