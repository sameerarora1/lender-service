package com.paytm.lender.service.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paytm.lender.model.config.RiskConfigEntity;
import com.paytm.lender.model.error.ErrorCode;
import com.paytm.lender.model.error.ErrorCodeEnum;
import com.paytm.lender.model.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
@Setter
@Getter
@Slf4j
public class PostpaidLoanDetailsCalculatorUtil {
    public static String calculateConvenienceFeeCategory(Double convFee) {

        String category = null;
        if (convFee == 0.0) {
            category = "B1";
        } else if (convFee <= 1.0) {
            category = "B2";
        } else if (convFee <= 2.0) {
            category = "B3";
        } else if (convFee <= 3.0) {
            category = "B4";
        } else {
            log.info("invalid conv fee");
            throw new BaseException(new ErrorCode(ErrorCodeEnum.INTERNAL_SERVER_ERROR));
        }
        return category;
    }


    public static String calculateSpendCategory(Double maxLoanAmount) {
        return "A" + (int) (maxLoanAmount / 250);
    }

    public static String calculateSpendCategoryMaxLimit(Double authorisedLimit, RiskConfigEntity spendCategoryCodeConfig) throws Exception {

        try {

            Double sanctionedLimit = authorisedLimit;
            if (authorisedLimit <= 20000) {
                sanctionedLimit = sanctionedLimit * 3;
            } else if (authorisedLimit > 20000 && authorisedLimit <= 40000) {
                sanctionedLimit = sanctionedLimit * 2.5;
            } else if (authorisedLimit > 40000 && authorisedLimit <= 60000) {
                sanctionedLimit = sanctionedLimit * 2;
            } else if (authorisedLimit > 60000 && authorisedLimit <= 100000) {
                sanctionedLimit = sanctionedLimit * 1.5;
            } else {
                sanctionedLimit = sanctionedLimit;
            }


            HashMap<String, String> spendCategoryCodeMap = new ObjectMapper()
                    .readValue(spendCategoryCodeConfig.getConfigValue(),
                            new TypeReference<HashMap<String, String>>() {
                            });
            if (null != spendCategoryCodeMap.get(String.valueOf(sanctionedLimit.intValue()))) {
                return spendCategoryCodeMap.get(String.valueOf(sanctionedLimit.intValue()));
            } else {
                throw new Exception("Spend category max limit code not found");
            }
        } catch (Exception e) {
            log.error("Exception while parsing risk config for exception {}",
                     e);
            throw new Exception("Exception while parsing risk config");
        }
    }

}
