package com.paytm.lender.service.utils;

import com.paytm.lender.service.config.HttpConfig;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Log4j2
public class HttpClient {

    @Autowired
    private HttpConfig httpConfig;

    public HttpComponentsClientHttpRequestFactory getHttpClientFactory() {
        final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory =
                new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(generateHttpClient());
        clientHttpRequestFactory.setReadTimeout(httpConfig.getSocketTimeout());
        clientHttpRequestFactory.setConnectionRequestTimeout(httpConfig.getConnectTimeout());
        clientHttpRequestFactory.setConnectTimeout(httpConfig.getConnectTimeout());
        return clientHttpRequestFactory;
    }

    private CloseableHttpClient generateHttpClient() {

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(httpConfig.getConnectTimeout())
                .setSocketTimeout(httpConfig.getSocketTimeout())
                .setConnectionRequestTimeout(httpConfig.getConnectTimeout())
                .setCookieSpec(CookieSpecs.STANDARD)
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(httpConfig.getTimeToLiveInMs(), TimeUnit.MILLISECONDS);
        connectionManager.setMaxTotal(httpConfig.getMaxTotal());
        connectionManager.setDefaultMaxPerRoute(httpConfig.getMaxPerRoute());

        return HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager)
                .build();
    }
}
