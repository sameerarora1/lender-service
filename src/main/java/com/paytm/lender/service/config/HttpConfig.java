package com.paytm.lender.service.config;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class HttpConfig {

    private Integer connectTimeout;
    private Integer socketTimeout;
    private Integer maxTotal;
    private Integer maxPerRoute;
    private Long timeToLiveInMs;
}
