package com.paytm.lender.service.config;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@ConfigurationProperties(prefix = "lender.httpconfig")
@PropertySources(value = {@PropertySource("classpath:lender-config.properties")})
@Getter
@Setter
@NoArgsConstructor
public class LenderHttpConfig extends HttpConfig {
    @Override
    public String toString() {
        return "LenderHttpConfig{} " + super.toString();
    }
}