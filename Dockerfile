#Package stage
FROM openjdk:8-jdk-alpine
COPY target/lender-service-*.jar lender-service.jar
EXPOSE 8080

ENTRYPOINT ["java","-jar","/lender-service.jar"]